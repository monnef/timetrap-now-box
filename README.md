# TNB: timetrap-now-box

This is a CLI program for showing currently running [TimeTrap](https://github.com/samg/timetrap) timer in a fractional format for [i3blocks](https://github.com/vivien/i3blocks) or any other bar.

This program is based on [an older script](https://gitlab.com/snippets/1900805).

![](screenshot.png)

# Installation

Requirements:

* [Stack](https://haskellstack.org)

```sh
$ stack install
```

# Example of i3blocks config

```ini
[timetrap]
label=⧗
command=timetrap-now-box "t n"
interval=5
```

# License
GPL-3
