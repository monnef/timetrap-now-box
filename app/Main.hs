{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}

module Main where

import UtilsTH

import Turtle hiding (match, printf)
import Prelude hiding (FilePath)
import qualified Data.Text as T
import Control.Lens.Regex.Text
import Control.Lens hiding ((<&>))
import Control.Arrow ((>>>))
import Text.Printf (printf)

data ExecResult = Running Text | NotRunning | Error Text
  deriving Show

tshow :: Show a => a -> Text
tshow = show >>> T.pack

tread :: Read a => Text -> a
tread = T.unpack >>> read

{-
*default: 0:37:44 (test)
*default: not running
*default: 0:16:43
-}
conv :: Text -> Text
conv x = x & matchWhole & processTop x
  where
    processTop :: Text -> [[Text]] -> Text
    processTop _ [[sheet, hours, minutes, seconds]] = textHmToTimePart hours minutes
    processTop _ [[sheet, hours, minutes, seconds, note]] = textHmToTimePart hours minutes <> " " <> note
    processTop input x = "match failed: " <> tshow x <> "; input = " <> tshow input
    matchWhole :: Text -> [[Text]]
    matchWhole x = x ^.. [rx|^\*([\p{L}\p{M}]+): (\d+):(\d+):(\d+) ?\(?((?:[\p{L}\p{M}\p{N}]|-|\+)*)\)?$|] . groups
    hmToFrac :: Int -> Int -> Double
    hmToFrac h m = fromIntegral h + fromIntegral m / 60.0
    textHmToTimePart :: Text -> Text -> Text
    textHmToTimePart hours minutes = hmToFrac (tread hours) (tread minutes) & formatTime
    formatTime :: Double -> Text
    formatTime = printf "%.2f" >>> T.pack

data CmdArgs =
  CmdArgs
    { _cmd :: Text
    }

getArgs :: Shell (Either Text CmdArgs)
getArgs = do
  args <- arguments
  if length args /= 1
    then return $ Left "Expecting exactly ONE argument - a command to run."
    else return $ Right $ CmdArgs { _cmd = head args }

runCmd :: CmdArgs -> Shell ExecResult
runCmd args = do
  tRes <- inshellWithErr (_cmd args) empty
  return $ case tRes of
    Left e ->
        if T.isInfixOf ": not running" (lineToText e)
          then NotRunning
          else Error (lineToText e)
    Right t -> Running $ t & lineToText

main = sh $ do
  argsE <- getArgs
  res <- case argsE of
    Left e -> return $ Error e
    Right args -> runCmd args
  case res of
    NotRunning -> echo "⨯"
    Error e -> echo $ e & unsafeTextToLine & ("ERR: " <>)
    Running x -> echo $ x & conv & unsafeTextToLine
  exit ExitSuccess
