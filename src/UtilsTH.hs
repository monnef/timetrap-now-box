module UtilsTH where

import Control.Lens.Regex.Text
import Text.Regex.PCRE.Light (utf8)

rx = mkRegexTraversalQQ [utf8]
